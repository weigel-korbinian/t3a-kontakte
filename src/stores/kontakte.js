import { reactive } from 'vue';
import { defineStore } from 'pinia';
import { v4 as uuidv4 } from 'uuid';


export const useKontakteStore = defineStore('kontakte', () => {
    // D A T A
    const kontaktlist = reactive([
        { id: 0, vorname: 'Anna', nachname: 'Arm', bot: true, gender: 'm' },
        { id: 1, vorname: 'Berta', nachname: 'Bein', bot: false, gender: 'w' },
        { id: 2, vorname: 'Carla', nachname: 'Copf', bot: true, gender: 'd' }
    ]);

    // C R E A T E
    function createKontakt(kontakt) {
        kontakt.id = uuidv4();
        kontaktlist.push(kontakt);
        save();
    }

    // U P D A T E
    function updateKontakt(updatedKontakt) {
        // Position des Kontakts mit der entsprechenden id finden
        const index = kontaktlist.findIndex(
            kontakt => kontakt.id === updatedKontakt.id
        );
        kontaktlist[index].vorname = updatedKontakt.vorname;
        kontaktlist[index].nachname = updatedKontakt.nachname;
        kontaktlist[index].bot = updatedKontakt.bot;
        kontaktlist[index].gender = updatedKontakt.gender;
        save();
    }

    // D E L E T E
    function deleteKontakt(id) {
        // Position des Kontakts mit der entsprechenden id finden
        const index = kontaktlist.findIndex(kontakt => kontakt.id === id);
        // Kontakt aus Array löschen
        kontaktlist.splice(index, 1);

        save();
    }

    // P E R S I S T E N C E
    function save() {
        const jsonString = JSON.stringify(kontaktlist);
        localStorage.setItem('T3A-Kontakte', jsonString);
    }

    function load() {
        if (localStorage.getItem('T3A-Kontakte')) {
            let dataString = localStorage.getItem('T3A-Kontakte');
            Object.assign(kontaktlist, JSON.parse(dataString));
        } else {
            Object.assign(kontaktlist, []);
        }
    }

    return { kontaktlist, createKontakt, deleteKontakt, updateKontakt, load };
});