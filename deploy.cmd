rd /s /q "dist"
call npm run build
echo "Build ist fertig"

cd dist
git init
git checkout -B pages
git add -A
git commit -m 'deploy'

git push --force -u https://codeberg.org/weigel-korbinian/t3a-kontakte.git pages
cd ..

